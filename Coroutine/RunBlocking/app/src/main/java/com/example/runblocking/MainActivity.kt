package com.example.runblocking

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class MainActivity : AppCompatActivity() {

    val TAG = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.d(TAG,"Before runBlocking")
        runBlocking {
            launch(Dispatchers.IO){
                delay(3000L)
                Log.d(TAG,"Finish IO Coroutine 1")
            }
            launch(Dispatchers.IO){
                delay(3000L)
                Log.d(TAG,"Finish IO Coroutine 2")
            }
            Log.d(TAG,"Start of runBlocking")
            delay(5000L)
            // Thread.sleep(5000L) // same result bhetato
            Log.d(TAG,"End of runBlocking")
        }
        Log.d(TAG,"After runBlocking")
    }
}